/******************************************************************************
 *  Copyright (C) 2016 by Dominik Schreiber <dev@dominikschreiber.de>         *
 *                                                                            *
 *  This library is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published         *
 *  by the Free Software Foundation; either version 3 of the License or (at   *
 *  your option) any later version.                                           *
 *                                                                            *
 *  This library is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
 *  Library General Public License for more details.                          *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this library; see the file LICENSE.                            *
 *  If not, see <http://www.gnu.org/licenses/>.                               *
 *****************************************************************************/

#include <iostream>

#include <KLocalizedString>
#include <KConfigCore/KConfig>
#include <KConfigCore/KConfigGroup>

#include <QApplication>
#include <QDesktopServices>
#include <QClipboard>
#include <QRegExp>
#include <QTextCodec>
#include <QtNetworkAuth>
#include <QJsonDocument>
#include <QJsonObject>
#include <QtDBus>

#include "spotify.h"
#include "tokens.h"

using namespace std;

Spotify::Spotify(QObject *parent, const QVariantList &args) : Plasma::AbstractRunner(parent, args), isGranted(false)
{
    Q_UNUSED(args);

    // General runner configuration
    setObjectName(QLatin1String("Spotify"));
    setHasRunOptions(true);
    // setIgnoredTypes(Plasma::RunnerContext::FileSystem | Plasma::RunnerContext::Help | Plasma::RunnerContext::NetworkLocation);
    setSpeed(AbstractRunner::SlowSpeed);
    setPriority(HighestPriority);
    const char *text = "Looks for a unicode symbol described by :q: and, if present, displays it. Then pressing ENTER copies the symbol to the clipboard.";
    setDefaultSyntax(Plasma::RunnerSyntax(QString::fromLatin1(":q:"), i18n(text)));
    loadConfig();
    sendNotification("Starting");
    auto replyHandler = new QOAuthHttpServerReplyHandler(8080, this);
    replyHandler->setCallbackPath("cb");
    spotify.setReplyHandler(replyHandler);
    spotify.setAuthorizationUrl(QUrl("https://accounts.spotify.com/authorize"));
    spotify.setAccessTokenUrl(QUrl("https://accounts.spotify.com/api/token"));
    spotify.setClientIdentifier(clientId);
    spotify.setClientIdentifierSharedKey(clientSecret);
    spotify.setScope("user-read-private user-top-read playlist-read-private playlist-modify-public playlist-modify-private");

    connect(&spotify, &QOAuth2AuthorizationCodeFlow::authorizeWithBrowser,
            &QDesktopServices::openUrl);

    connect(&spotify, &QOAuth2AuthorizationCodeFlow::statusChanged,
            this, &Spotify::authStatusChanged);

    connect(&spotify, &QOAuth2AuthorizationCodeFlow::granted,
            this, &Spotify::granted);
    on_actionGrant_triggered();
}

Spotify::~Spotify()
{
    spotify.disconnect();
}

void Spotify::loadConfig()
{

    // Global configuration (meant to be immutable by user)
    KConfig globalConfig("/usr/share/config/krunner-symbolsrc");
    // Local configuration with custom definitions and preferences
    KConfig localConfig("krunner-symbolsrc", KConfig::SimpleConfig);

    // Symbol definitions
    KConfigGroup globalDefGroup(&globalConfig, "Definitions");
    QMap<QString, QString> globalDefMap = globalDefGroup.entryMap();
    KConfigGroup localDefGroup(&localConfig, "Definitions");
    QMap<QString, QString> localDefMap = localDefGroup.entryMap();
    mergeMapsOverriding(&globalDefMap, &localDefMap);
    symbols = localDefMap;

    // Find and change definitions of the form "a,b,c=d"
    // to "a=d", "b=d", "c=d"
    expandMultiDefinitions();

    // Preferences configuration
    KConfigGroup globalPrefGroup(&globalConfig, "Preferences");
    QMap<QString, QString> globalPrefMap = globalPrefGroup.entryMap();
    KConfigGroup localPrefGroup(&localConfig, "Preferences");
    QMap<QString, QString> localPrefMap = localPrefGroup.entryMap();
    mergeMapsOverriding(&globalPrefMap, &localPrefMap);
    QMap<QString, QString> prefMap = localPrefMap;

    // // UseUnicodeDatabase: Default true
    // prefs.insert("UseUnicodeDatabase",
    //              (!prefMap.contains("UseUnicodeDatabase")) || prefMap.value("UseUnicodeDatabase").compare("true") == 0);

    // // Unicode Spotify (only if not disabled by preferences)
    // if (prefs.value("UseUnicodeDatabase").toBool())
    // {
    //     KConfig unicodeConfig("/usr/share/config/krunner-Spotify-full-unicode-index");
    //     KConfigGroup unicodeGroup(&unicodeConfig, "Unicode");
    //     unicodeSymbols = unicodeGroup.entryMap();
    // }
}

void Spotify::match(Plasma::RunnerContext &context)
{
    if (!context.isValid())
        return;

    const QString enteredKey = context.query();

    QList<Plasma::QueryMatch> matches;
    QMapIterator<QString, QString> it(symbols);

    if (enteredKey.startsWith("spotify:"))
    {
        QStringRef filteredKey(&enteredKey, 8, enteredKey.length() - 8);
        auto trimmedKey = filteredKey.trimmed();
        while (it.hasNext())
        {
            it.next();
            QString foundKey = it.key();
            if (foundKey.startsWith(trimmedKey))
            {
                // We have a match
                Plasma::QueryMatch match(this);

                if (foundKey.length() == enteredKey.length())
                {
                    // the query equals the keyword -> exact match
                    match.setType(Plasma::QueryMatch::ExactMatch);
                }
                else
                {
                    // the query is a (non-complete) prefix of the keyword
                    // -> completion match
                    match.setType(Plasma::QueryMatch::CompletionMatch);
                }
                // also show the exact keyword for this value
                match.setText(it.value());
                match.setSubtext("[" + foundKey + "]");

                match.setData(it.value());

                // Basic properties for the match
                match.setIcon(QIcon::fromTheme("preferences-desktop-font"));
                //match.setSubtext(it.value());

                // The match's relevance gets higher the more "complete" the query string is
                // (k/x for query length k and keyword length x; 1 for complete keyword)
                match.setRelevance((float)enteredKey.length() / (float)foundKey.length());

                matches.append(match);
            }
        }
    }
    else
    {
        QUrl u("https://api.spotify.com/v1/search?q=" + enteredKey + "&type=playlist,track&limit=5&market=from_token");

        auto reply = spotify.get(u);

        connect(reply, &QNetworkReply::finished, [=]() {
            if (reply->error() != QNetworkReply::NoError)
            {
                sendNotification(reply->errorString());
                return;
            }
            const auto data = reply->readAll();
            Spotify::sendNotification(data);

            // const auto document = QJsonDocument::fromJson(data);
            // const auto root = document.object();

            reply->deleteLater();
        });
    }

    // Feed the framework with the calculated results
    context.addMatches(matches);
}

/**
 * Perform an action when a user chooses one of the previously found matches.
 * Either some string gets copied to the clipboard, a file/path/URL is being opened, 
 * or a command is being executed.
 */
void Spotify::run(const Plasma::RunnerContext &context, const Plasma::QueryMatch &match)
{
    Q_UNUSED(context);

    if (match.data().toString().compare("grant") == 0)
    {
        // Open a file or a URL in a (file/web) browser
        // (in a new process, so that krunner doesn't get stuck while opening the path)
        // string command = "kde-open " + match.text().remove("→ ").toStdString() + " &";
        // system(command.c_str());
        on_actionGrant_triggered();
    }
    else if (match.data().toString().compare("execute") == 0)
    {
        // Execute a command
        // (in a new process, so that krunner doesn't get stuck while opening the path)
        string command = match.text().remove(">_ ").toStdString() + " &";
        system(command.c_str());
    }
    else
    {
        // Copy the result to clipboard
        QApplication::clipboard()->setText(match.text());
    }
}

/*
 * Stores the union of two QMap<QString,QString> objects inside the second map, 
 * whereas an entry of the overriding map will be preferred (and the overridden map's 
 * one will be ignored) in case of duplicate keys.
 */
void Spotify::mergeMapsOverriding(QMap<QString, QString> *overriddenMap, QMap<QString, QString> *overridingMap)
{
    QMapIterator<QString, QString> it(*overriddenMap);
    while (it.hasNext())
    {
        it.next();
        if (!overridingMap->contains(it.key()))
        {
            overridingMap->insert(it.key(), it.value());
        }
    }
}

/*
 * Iterates over all symbol definitions and looks for definitions of the kind
 * "a,b,c=d". These definitions are expanded to seperate definitions of the 
 * form "a=d", "b=d", "c=d".
 */
void Spotify::expandMultiDefinitions()
{

    QMap<QString, QString> splittedSymbols;
    QStringList keysToRemove; // expanded keys are stored herein
    QMapIterator<QString, QString> it(symbols);
    QRegExp exp("^(.+,)+.+$"); // regex for multiple keys

    // For each known symbol
    while (it.hasNext())
    {
        it.next();
        QString key = it.key();
        int pos = exp.indexIn(key, 0);
        if (pos >= 0)
        {

            // Multi definition found
            QStringList keywords = key.split(",");
            QStringListIterator strIt(keywords);
            while (strIt.hasNext())
            {

                // Add a new symbol entry for each part of the key
                QString newKey = strIt.next();
                splittedSymbols.insert(newKey, it.value());
            }
            // The old key is to be deleted
            keysToRemove.append(key);
        }
    }
    // Remove old keys
    QStringListIterator strIt(keysToRemove);
    while (strIt.hasNext())
    {
        symbols.remove(strIt.next());
    }

    // Merge the ney Spotify into the map with all Spotify
    symbols.unite(splittedSymbols);
}

void Spotify::granted()
{
    sendNotification("Signal granted received");

    QString token = spotify.token();
    sendNotification("Token: " + token, "Spotify Auth Success");

    // ui->actionGet_Playlists->setEnabled(true);
    // ui->actionGet_User_Information->setEnabled(true);
    isGranted = true;
}

void Spotify::authStatusChanged(QAbstractOAuth::Status status)
{
    QString s;
    if (status == QAbstractOAuth::Status::Granted)
        s = "granted";

    if (status == QAbstractOAuth::Status::TemporaryCredentialsReceived)
    {
        s = "temp credentials";
        //oauth2.refreshAccessToken();
    }

    sendNotification("Auth Status changed: " + s + "\n");
}

void Spotify::on_actionGrant_triggered()
{
    spotify.grant();
}

void Spotify::on_actionGet_User_Information_triggered()
{
    sendNotification("Loading User Informations");

    QUrl u("https://api.spotify.com/v1/me");

    auto reply = spotify.get(u);

    connect(reply, &QNetworkReply::finished, [=]() {
        Spotify::sendNotification("Got Result");
        if (reply->error() != QNetworkReply::NoError)
        {
            sendNotification(reply->errorString());
            return;
        }
        const auto data = reply->readAll();
        sendNotification(data);

        const auto document = QJsonDocument::fromJson(data);
        const auto root = document.object();
        userName = root.value("id").toString();

        sendNotification("Username: " + userName);

        reply->deleteLater();
    });
}

void Spotify::on_actionGet_Playlists_triggered()
{
    if (userName.length() == 0)
        return;

    sendNotification("Loading Playlists ...");

    QUrl u("https://api.spotify.com/v1/users/" + userName + "/playlists");

    auto reply = spotify.get(u);

    connect(reply, &QNetworkReply::finished, [=]() {
        if (reply->error() != QNetworkReply::NoError)
        {
            sendNotification(reply->errorString());
            return;
        }

        const auto data = reply->readAll();

        sendNotification(data);

        reply->deleteLater();
    });
}

void Spotify::sendNotification(QString text, QString title)
{
    QDBusInterface iface("org.freedesktop.Notifications", "/org/freedesktop/Notifications", "org.freedesktop.Notifications", QDBusConnection::sessionBus(), this);
    QList<QVariant> args;
    args << "Spotify KRunner"
         << (uint)19678654
         << ""
         << title
         << text
         << QStringList()
         << QMap<QString, QVariant>()
         << -1;
    auto reply = iface.callWithArgumentList(QDBus::CallMode::Block, "Notify", args);
    if (((QDBusReply<uint>)reply).isValid())
    {
        qDebug() << "\033[1;33mNotify successful:" << (QDBusReply<uint>)reply << "\033[0m";
    }
    else
    {
        qCritical() << "\033[31mNotify failed:" << reply << "\033[0m";
    }
}

K_EXPORT_PLASMA_RUNNER(spotify, Spotify)

#include "spotify.moc"