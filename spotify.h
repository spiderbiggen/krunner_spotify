/******************************************************************************
 *  Copyright (C) 2016 by Dominik Schreiber <dev@dominikschreiber.de>         *
 *                                                                            *
 *  This library is free software; you can redistribute it and/or modify      *
 *  it under the terms of the GNU General Public License as published         *
 *  by the Free Software Foundation; either version 3 of the License or (at   *
 *  your option) any later version.                                           *
 *                                                                            *
 *  This library is distributed in the hope that it will be useful,           *
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of            *
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU         *
 *  Library General Public License for more details.                          *
 *                                                                            *
 *  You should have received a copy of the GNU General Public License         *
 *  along with this library; see the file LICENSE.                            *
 *  If not, see <http://www.gnu.org/licenses/>.                               *
 *****************************************************************************/

#ifndef SPOTIFY_H
#define SPOTIFY_H

#include <KRunner/AbstractRunner>
#include <QtNetworkAuth>
#include <QOAuth2AuthorizationCodeFlow>

class Spotify : public Plasma::AbstractRunner
{
    Q_OBJECT

public:
    Spotify(QObject *parent, const QVariantList &args);
    ~Spotify();

    void match(Plasma::RunnerContext &) override;
    void run(const Plasma::RunnerContext &, const Plasma::QueryMatch &) override;

private slots:
    void granted();

    void authStatusChanged (QAbstractOAuth::Status status);
    void on_actionGrant_triggered();

    void on_actionGet_User_Information_triggered();

    void on_actionGet_Playlists_triggered();

    // void onSpotifySearchResults(Plasma::RunnerContext &context, QNetworkReply &reply);
    

private:
    QMap<QString, QString> symbols;
    QMap<QString, QString> unicodeSymbols;
    QMap<QString, QVariant> prefs;

    QOAuth2AuthorizationCodeFlow spotify;
    bool isGranted;
    QString userName;

    void loadConfig();
    void matchUnicode(Plasma::RunnerContext &);

    // helper methods
    void mergeMapsOverriding(QMap<QString, QString> *overriddenMap, QMap<QString, QString> *overridingMap);
    void expandMultiDefinitions();
    void sendNotification(QString text, QString title = "Spotify Krunner");
};

// class SearchQuery {
// public:
//     SearchQuery();
//     ~SearchQuery();


// private:


// };

#endif
